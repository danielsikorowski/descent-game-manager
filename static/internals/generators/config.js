// Define the structure of the Project

const Structure = {
  components: ['Common', 'HTML', 'Partials', 'Routing'],
  containers: ['Blocks', 'Providers', 'Views', 'Root'],
};

module.exports = Structure;
