/**
 * Component Generator
 */

/* eslint strict: ["off"] */

'use strict';
const { isNotStaticFolder, componentExists, folderExists, isComponentDirectory, CONSTANT} =  require('../utils');

module.exports = {
  description: 'Add an unconnected component',
  prompts: [
    {
      type: 'list',
      name: 'type',
      message: 'Select the type of component',
      default: 'Stateless Function',
      choices: () => CONSTANT.component.classes,
    },
    {
      type: 'auto-suggest-list',
      name: 'fileFolderPath',
      message: 'Specify path for this component',
      default: '',
      pathFilter: (isDirectory, nodePath) => isDirectory && isNotStaticFolder(nodePath, true) && isComponentDirectory(nodePath),
      rootPath: CONSTANT.component.cleanPath,
      suggestOnly: false,
      pageSize: 10,
    },
    {
      type: 'input',
      name: 'newOptionalFolder',
      message: 'Write new folder name or leave empty',
      default: '',
      validate: (value, answers) => {
        if (/.+/.test(value)) {
          return folderExists(value, answers.fileFolderPath)
            ? 'A folder with this name already exists'
            : true;
        }
        return true;
      },
    },
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'Button',
      validate: value => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? 'A component or container with this name already exists'
            : true;
        }

        return 'The name is required';
      },
    },
    {
      type: 'confirm',
      name: 'wantMessages',
      default: true,
      message: 'Do you want i18n messages (i.e. will this component use text)?',
    },
    {
      type: 'confirm',
      name: 'wantLoadable',
      default: false,
      message: 'Do you want to load the component asynchronously?',
    },
  ],
  actions: data => {
    // Generate index.js and index.test.js
    let componentTemplate;
    let specificFolderPath = '';

    switch (data.type) {
      case 'Stateless Function': {
        componentTemplate = './component/stateless.js.hbs';
        break;
      }
      default: {
        componentTemplate = './component/class.js.hbs';
      }
    }

    if (data.newOptionalFolder !== '') {
      specificFolderPath = data.newOptionalFolder + '/';
    }

    let fullFileFolderPath = data.fileFolderPath + '/' + specificFolderPath;

    const actions = [
      {
        type: 'add',
        path:
          '../../' + fullFileFolderPath + '{{properCase name}}/index.js',
        templateFile: componentTemplate,
        abortOnFail: true,
      },
      {
        type: 'add',
        path:
          '../../' + fullFileFolderPath + '{{properCase name}}/tests/index.test.js',
        templateFile: './component/test.js.hbs',
        abortOnFail: true,
      },
    ];

    console.log(data.fileFolderPath);
    if (data.fileFolderPath === 'app\\components\\html') {
      actions.push({
        type: 'append',
        path: '../../app/components/html/index.js',
        pattern: '// <IMPORT_NEW_COMPONENT_HERE>',
        template: "import {{camelCase name}} from './" + specificFolderPath + "{{properCase name}}';",
      });
      actions.push({
        type: 'append',
        path: '../../app/components/html/index.js',
        pattern: '// <EXPORT_NEW_COMPONENT_HERE>',
        template: 'export const {{properCase name}} = {{camelCase name}};',
      });
    }

    // If they want a i18n messages file
    if (data.wantMessages) {
      actions.push({
        type: 'add',
        path:
          '../../' + fullFileFolderPath + '{{properCase name}}/messages.js',
        templateFile: './component/messages.js.hbs',
        abortOnFail: true,
      });
    }

    // If want Loadable.js to load the component asynchronously
    if (data.wantLoadable) {
      actions.push({
        type: 'add',
        path:
          '../../' + fullFileFolderPath + '{{properCase name}}/Loadable.js',
        templateFile: './component/loadable.js.hbs',
        abortOnFail: true,
      });
    }

    actions.push({
      type: 'prettify',
      path: '/components/',
    });

    return actions;
  },
};
