const { getDirectories } = require('./getDirectories');
const { isUndefined, find } = require('lodash');

module.exports = (value, path) => {
  const dictionaries = getDirectories(path);
  return !isUndefined(find(dictionaries, value));
};
