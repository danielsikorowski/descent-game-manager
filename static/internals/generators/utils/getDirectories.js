/* eslint strict: ["off"] */

'use strict';
const isNotStaticFolder = require('./isNotStaticFolder');
const { readdirSync, lstatSync, statSync } = require('fs');
const { join } = require('path');

const isDirectory = source => lstatSync(source.path).isDirectory();
const getDirectories = source =>
  readdirSync(source)
    .map(name => { return {path: join(source, name), name}})
    .filter(isDirectory)
    .map(source => source.name);

const walkSync = function(dir, filelist, staticPath) {
    let files = readdirSync(dir);

    files.forEach(function(file) {
      let filePath = dir + '/' + file;
      if (statSync(filePath).isDirectory() && isNotStaticFolder(file)) {
        let dirName = (filePath).replace(staticPath, '');
        filelist.push(dirName);
        filelist = walkSync(filePath, filelist, staticPath);
      }
  });

  return filelist;
};

const getTreeDirectories = source => walkSync(source, [], source);

module.exports = {
  getDirectories,
  getTreeDirectories
};
