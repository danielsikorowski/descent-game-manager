const folderExists =  require('./folderExists');
const { getDirectories, getTreeDirectories } = require('./getDirectories');
const notAStaticFolder = require('./isNotStaticFolder');
const componentExists = require('./componentExists');
const isCurrentDirectory = require('./isCurrentDirectory');

const CONSTANT = {
  component: {
    path: './app',
    cleanPath: 'app',
    classes: [
      'Stateless Function',
      'React.PureComponent',
      'React.Component',
    ]
  },
  container: {
    path: './app' ,
    cleanPath: 'app',
    classes: [
      'Stateless Function',
      'React.PureComponent',
      'React.Component',
    ]
  },
  staticFolders: ['tests'],
};
const isNotStaticFolder = notAStaticFolder(CONSTANT.staticFolders);
const isComponentDirectory = isCurrentDirectory(CONSTANT.component.cleanPath);
const isContainersDirectory = isCurrentDirectory(CONSTANT.container.cleanPath);

module.exports = {
  folderExists,
  getDirectories,
  getTreeDirectories,
  componentExists,
  isComponentDirectory,
  isContainersDirectory,
  isNotStaticFolder,
  CONSTANT
};
