module.exports = (currentDir) => (dirName) => {
   return currentDir !== dirName;
};
