const { find, words, isEmpty, intersection } = require('lodash');

module.exports = (staticFolders) => (dirName, isPath) => {
    if (isPath) {
      return isEmpty(intersection(staticFolders, words(dirName, /[^\\]+/g)));
    }
    return !find(staticFolders, dirName);
};
