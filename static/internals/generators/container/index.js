/**
 * Container Generator
 */
const { isNotStaticFolder, componentExists, folderExists, isContainersDirectory, CONSTANT} =  require('../utils');

module.exports = {
  description: 'Add a container component',
  prompts: [
    {
      type: 'list',
      name: 'type',
      message: 'Select the base component type:',
      default: 'Stateless Function',
      choices: () => CONSTANT.container.classes,
    },
    {
      type: 'auto-suggest-list',
      name: 'fileFolderPath',
      message: 'Specify path for this component',
      default: '',
      pathFilter: (isDirectory, nodePath) => isDirectory && isNotStaticFolder(nodePath, true) && isContainersDirectory(nodePath),
      rootPath: CONSTANT.container.cleanPath,
      suggestOnly: false,
      pageSize: 10,
    },
    {
      type: 'input',
      name: 'newOptionalFolder',
      message: 'Write new folder name or leave empty',
      default: '',
      validate: (value, answers) => {
        if (/.+/.test(value)) {
          return folderExists(value, answers.fileFolderPath)
            ? 'A folder with this name already exists'
            : true;
        }
        return true;
      },
    },
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'Form',
      validate: value => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? 'A component or container with this name already exists'
            : true;
        }

        return 'The name is required';
      },
    },
    {
      type: 'confirm',
      name: 'wantHeaders',
      default: false,
      message: 'Do you want headers?',
    },
    {
      type: 'confirm',
      name: 'wantActionsAndReducer',
      default: true,
      message:
        'Do you want an actions/constants/selectors/reducer tuple for this container?',
    },
    {
      type: 'confirm',
      name: 'wantSaga',
      default: true,
      message: 'Do you want sagas for asynchronous flows? (e.g. fetching data)',
    },
    {
      type: 'confirm',
      name: 'wantMessages',
      default: true,
      message: 'Do you want i18n messages (i.e. will this component use text)?',
    },
    {
      type: 'confirm',
      name: 'wantLoadable',
      default: true,
      message: 'Do you want to load resources asynchronously?',
    },
  ],
  actions: data => {
    // Generate index.js and index.test.js
    let componentTemplate; // eslint-disable-line no-var
    let specificFolderPath = '';

    switch (data.type) {
      case 'Stateless Function': {
        componentTemplate = './container/stateless.js.hbs';
        break;
      }
      default: {
        componentTemplate = './container/class.js.hbs';
      }
    }

    if (data.newOptionalFolder !== '') {
      specificFolderPath = data.newOptionalFolder + '/';
    }

    let fullFileFolderPath = data.fileFolderPath + '/' + specificFolderPath;

    const actions = [
      {
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/index.js',
        templateFile: componentTemplate,
        abortOnFail: true,
      },
      {
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/tests/index.test.js',
        templateFile: './container/test.js.hbs',
        abortOnFail: true,
      },
    ];

    // If component wants messages
    if (data.wantMessages) {
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/messages.js',
        templateFile: './container/messages.js.hbs',
        abortOnFail: true,
      });
    }

    // If they want actions and a reducer, generate actions.js, constants.js,
    // reducer.js and the corresponding tests for actions and the reducer
    if (data.wantActionsAndReducer) {
      // Actions
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/actions.js',
        templateFile: './container/actions.js.hbs',
        abortOnFail: true,
      });
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/tests/actions.test.js',
        templateFile: './container/actions.test.js.hbs',
        abortOnFail: true,
      });

      // Constants
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/constants.js',
        templateFile: './container/constants.js.hbs',
        abortOnFail: true,
      });

      // Selectors
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/selectors.js',
        templateFile: './container/selectors.js.hbs',
        abortOnFail: true,
      });
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/tests/selectors.test.js',
        templateFile: './container/selectors.test.js.hbs',
        abortOnFail: true,
      });

      // Reducer
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/reducer.js',
        templateFile: './container/reducer.js.hbs',
        abortOnFail: true,
      });
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/tests/reducer.test.js',
        templateFile: './container/reducer.test.js.hbs',
        abortOnFail: true,
      });
    }

    // Sagas
    if (data.wantSaga) {
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/saga.js',
        templateFile: './container/saga.js.hbs',
        abortOnFail: true,
      });
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/tests/saga.test.js',
        templateFile: './container/saga.test.js.hbs',
        abortOnFail: true,
      });
    }

    if (data.wantLoadable) {
      actions.push({
        type: 'add',
        path:
          '../../'  + fullFileFolderPath + '{{properCase name}}/Loadable.js',
        templateFile: './component/loadable.js.hbs',
        abortOnFail: true,
      });
    }

    actions.push({
      type: 'prettify',
      path: '/containers',
    });

    return actions;
  },
};
