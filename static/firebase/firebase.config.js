import firebase from 'firebase';

// Initialize Firebase
export const app = firebase.initializeApp({
  apiKey: "AIzaSyCPv0trwXRx2txeDZ_g9i-Ag9f_aqpcmvo",
  authDomain: "descent-game-manager.firebaseapp.com",
  databaseURL: "https://descent-game-manager.firebaseio.com",
  projectId: "descent-game-manager",
  storageBucket: "descent-game-manager.appspot.com",
  messagingSenderId: "792052252321"
});
