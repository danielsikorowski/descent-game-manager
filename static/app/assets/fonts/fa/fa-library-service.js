import { library } from '@fortawesome/fontawesome-svg-core';
// define all needed Icons
import {
  faStroopwafel, faUnlockAlt, faUserCircle, faDungeon, faSkull,
  faCog, faPowerOff, faChartLine, faTools, faUserCog, faUserNinja,
  faSearchLocation, faJournalWhills, faScroll} from '@fortawesome/free-solid-svg-icons';

// append them to library
library.add(
  faStroopwafel,
  faUnlockAlt,
  faUserCircle,
  faDungeon,
  faSkull,
  faCog,
  faPowerOff,
  faChartLine,
  faTools,
  faUserCog,
  faUserNinja,
  faSearchLocation,
  faJournalWhills,
  faScroll
);
