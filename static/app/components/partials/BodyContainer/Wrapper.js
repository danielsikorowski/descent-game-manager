import styled from 'styled-components';

const BodyContainerWrapper = styled.div`
  width: 100%;
  position: absolute;
  top: 100px;
  left: 0;
  right: 0;
  bottom: 0;
  background: #f1f1f1;
`;

export default BodyContainerWrapper;
