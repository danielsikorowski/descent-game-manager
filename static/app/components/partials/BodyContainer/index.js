/**
 *
 * BodyContainer
 *
 */

import React from 'react';
import Wrapper from "./Wrapper";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function BodyContainer(props) {
  return (
    <Wrapper>
      {props.children}
    </Wrapper>
  );
}

BodyContainer.propTypes = {};

export default BodyContainer;
