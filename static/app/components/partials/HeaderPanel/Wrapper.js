import styled from 'styled-components';

const HeaderPanelWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100px;
  background: #c5c5c5;
  position: relative;
  z-index: 2;
`;

export default HeaderPanelWrapper;
