/**
 *
 * HeaderPanel
 *
 */

import React from 'react';
import Wrapper from './Wrapper';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
// {/*<NavigationBar />*/}

import UserPanel from 'containers/blocks/headerPanel/UserPanel';
import AppLogo from "containers/blocks/headerPanel/AppLogo/index";
import NavigationPanel from "containers/blocks/headerPanel/NavigationPanel/index";

function HeaderPanel() {
  return (
    <Wrapper>
      <AppLogo />
      <UserPanel />
      <NavigationPanel />
    </Wrapper>
  );
}

HeaderPanel.propTypes = {};

export default HeaderPanel;
