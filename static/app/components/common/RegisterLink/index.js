/**
 *
 * RegisterForm
 *
 */

import React from 'react';
import { Link } from 'react-router-dom';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import {TITLE_BOLD} from '../../../assets/fonts/text/index'
const StyledLink = styled(Link)`
  font-family: ${TITLE_BOLD};
  font-size: larger;
`;

const StyledSmall = styled.div`
  font-size: 80%;
  margin-bottom: 10px;
`;

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function RegisterLink() {
  return (
    <div>
      <StyledSmall className="text-muted">
        You are not registered as a Hero yet?
      </StyledSmall>
      <StyledLink to="/register">
        Register
      </StyledLink>
    </div>
  );
}

RegisterLink.propTypes = {};

export default RegisterLink;
