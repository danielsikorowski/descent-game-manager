/**
 *
 * UserAvatar
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

import avatar from 'assets/images/avatar-dezra.png';

const AvatarImg = styled.img`
  border: 1px solid #800e0e;
  border-radius: 50%;
  height: 65px;
  width: 65px;
  -webkit-box-shadow: 2px -7px 18px 8px rgba(0,0,0,0.75); 
  -moz-box-shadow: 2px -7px 18px 8px rgba(0,0,0,0.75);
  box-shadow: 1px -3px 11px 5px rgba(0,0,0,0.75);
  transform: translateY(2px);
`;

const AvatarImgWrapper = styled.div`
  margin-left: 5px;
  display: inline-block;
  width: auto;
  padding: 10px;
  border-radius: 50%;
  background: #c53636;
  border: 1px solid brown;
  -webkit-box-shadow: -1px 3px 11px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: -1px 3px 11px 0px rgba(0,0,0,0.75);
  box-shadow: -1px 3px 11px 0px rgba(0,0,0,0.75);
`;

const AvatarImgContainer = styled.div`
  display: inline-block;
`;

function UserAvatar(props) {
  return (
    <AvatarImgContainer>
      <AvatarImgWrapper>
        <AvatarImg src={avatar}/>
      </AvatarImgWrapper>
    </AvatarImgContainer>
  );
}

UserAvatar.propTypes = {};

export default UserAvatar;
