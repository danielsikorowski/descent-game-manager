/**
 *
 * ErrorMessage
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function ErrorMessage(props) {
  return (
    <div className="alert alert-danger" role="alert">
      { props.error }
    </div>
  );
}

ErrorMessage.propTypes = {};

export default ErrorMessage;
