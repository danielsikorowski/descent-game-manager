/**
 *
 * DescentLogo
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';
import DescentLogoImage from 'assets/images/descent-title.png';

function DescentLogo(props) {
  const StyledDescentLogo = styled.img`
    width: ${props => props.width || 'initial'};
    height: ${props => props.height || 'initial'};
  `;

  return (
    <StyledDescentLogo {...props} src={DescentLogoImage} />
  );
}

DescentLogo.propTypes = {};

export default DescentLogo;
