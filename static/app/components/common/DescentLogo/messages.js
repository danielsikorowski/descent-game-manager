/*
 * DescentLogo Messages
 *
 * This contains all the text for the DescentLogo component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DescentLogo.header',
    defaultMessage: 'This is the DescentLogo component !',
  },
});
