/**
 *
 * PrivateRoute
 *
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';

import { makeSelectLocation } from 'containers/root/App/selectors';

import auth from '../../../containers/root/AuthFlow/auth';
import HeaderPanel from '../../partials/HeaderPanel';
import BodyContainer from '../../partials/BodyContainer';
import SidebarPanel from "../../../containers/blocks/sidebarPanel";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const displayGameSidebarPanel = rest.config.gameSidebarPanel;
  const redirectSetLocation = () => {
    console.log(rest);
    // state: { from: props.location } }}
    return true;
  };

  return (
    <Route
      {...rest}
      render={props =>
        // fixme: use Store instead of service??
        auth.isLoggedIn() ? (
          <Fragment>
            <HeaderPanel />
            <BodyContainer>
              <Component {...props} />
              {displayGameSidebarPanel && <SidebarPanel />}
            </BodyContainer>
          </Fragment>
        ) : (
          redirectSetLocation() && <Redirect to="/login" />
        )
      }
    />
  );
};
PrivateRoute.propTypes = {};

const mapStateToProps = createStructuredSelector({
  location: makeSelectLocation(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PrivateRoute);
