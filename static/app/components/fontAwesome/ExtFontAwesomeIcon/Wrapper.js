import styled from 'styled-components';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome/index.es";

const ExtFontAwesomeIconWrapper = styled(FontAwesomeIcon)`
  
`;

export default ExtFontAwesomeIconWrapper;
