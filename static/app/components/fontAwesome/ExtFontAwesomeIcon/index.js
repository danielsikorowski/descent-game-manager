/**
 *
 * ExtFontAwesomeIcon
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import FontAwesomeIconWrapper from './Wrapper';

function ExtFontAwesomeIcon(props) {
  return (
    <FontAwesomeIconWrapper icon={props.icon} />
  );
}

ExtFontAwesomeIcon.propTypes = {};

export default ExtFontAwesomeIcon;
