/**
 *
 * Label
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';


function Label(props) {
  let labelElement;

  if (props.type === 'joined') {
    labelElement = (
      <div className="input-group-prepend">
        <label className="input-group-text" htmlFor={props.htmlFor} style={{width: '40px'}}>
          {props.children}
        </label>
      </div>
    );
  } else {
    labelElement = (
      <label {...props}>
        {props.children}
      </label>
    );
  }
  return labelElement;
}

Label.propTypes = {};

export default Label;
