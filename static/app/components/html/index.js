// <IMPORT_NEW_COMPONENT_HERE>
import label from './Label';
import extInput from './ExtInput';
import input from './Input';
import a from './A';
import button from './Button';
import img from './Img';
import h1 from './H1';
import h2 from './H2';
import h3 from './H3';

// <EXPORT_NEW_COMPONENT_HERE>
export const Label = label;
export const ExtInput = extInput;
export const Input = input;
export const A = a;
export const Button = button;
export const Img = img;
export const H1 = h1;
export const H2 = h2;
export const H3 = h3;
