import React from 'react';
import PropTypes from 'prop-types';

import Link from './Link';

function Button(props) {
  switch (props.type) {

    // Render link for router
    case 'link':
      return (
        <button className={props.className}>
          <Link to={props.to}>
            {props.children}
          </Link>
        </button>
      );
      break;

    // Render an anchor tag
    case 'anchor':
      return (
        <a href={props.href} className={props.className} onClick={props.onClick}>
          {props.children}
        </a>
      );
    break;

    // if type is not defined, then we want to render a button
    default:
      return (
        <button className={props.className} onClick={props.onClick} type={props.type}>
          {props.children}
        </button>
      );
  }
}

Button.propTypes = {
  href: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default Button;
