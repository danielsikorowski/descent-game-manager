const defaultConfig = {
  settings: {
    type: "text",
    component: "labeled",
    properties: {
      position: 'left',
      type:'joined'
    },
  },
  fieldProps: {
    autoCorrect: 'off',
    autoCapitalize: 'off',
    spellCheck: 'false'
  }
};

export default defaultConfig;
