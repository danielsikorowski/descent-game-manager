import { defaultsDeep } from 'lodash';
import defaultConfig from './defaultConfig';

const setFieldConfiguration = config => defaultsDeep({}, config, defaultConfig);

const username = setFieldConfiguration({
  fieldProps: {
    id: 'username',
    placeholder: 'frank.underwood',
  },
  icon: 'user-circle'
});

const password = setFieldConfiguration({
  settings: {
    type: "password"
  },
  fieldProps: {
    id: 'username',
    placeholder: 'frank.underwood'
  },
  icon: 'unlock-alt'
});

const email = setFieldConfiguration({

});

const name = setFieldConfiguration({

});

export default {
  username,
  password
}
