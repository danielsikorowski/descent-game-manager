import React, { Fragment } from 'react';
import Input from "../../Input"
import Label from "../../Label";

export const LabeledInput = (props, config) => {
  let {settings, fieldProps} = config;
  let {properties} = settings;
  let InputForm;

  if (properties.position === 'left') {
    InputForm =
      (<Fragment>
        <Label htmlFor={fieldProps.id} type={properties.type}>
          {props.children}
        </Label>
        <Input className="form-control" type={settings.type} {...props}/>
      </Fragment>);
  } else {
    InputForm =
      (<Fragment>
        <Input className="form-control" type={settings.type} {...props}/>
        <Label htmlFor={fieldProps.id} type={properties.type}>
          {props.children}
        </Label>
      </Fragment>);
  }

  return InputForm;
};
