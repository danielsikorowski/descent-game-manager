/**
 *
 * ExtInput
 *
 */

import React, {Fragment} from 'react';
import {LabeledInput} from './types';
import ExtInputConfig from './config';

// import PropTypes from 'prop-types';
// import styled from 'styled-components';


const ExtInput = (props) => {
  let inputComponent;
  let {component} = props.config.settings;
  let config = props.config;

  switch (component) {
    case 'labeled':
      inputComponent = LabeledInput(props, config);
  }

  return (
    <Fragment>
      {inputComponent}
    </Fragment>
  );
};

ExtInput.propTypes = {};

export default ExtInput;
export {
  ExtInputConfig
};
