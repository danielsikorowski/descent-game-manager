/**
 *
 * Input
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import * as _ from 'lodash';


function Input(props) {
  // todo: maybe some kind of util service to strip prop for void tags
  let inputProps = _.omit(props, ['children', 'config']);

  return (
    <input {...inputProps} />
  );
}

Input.propTypes = {};

export default Input;
