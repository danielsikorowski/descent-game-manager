import { fromJS } from 'immutable';
import sidebarPanelReducer from '../reducer';

describe('sidebarPanelReducer', () => {
  it('returns the initial state', () => {
    expect(sidebarPanelReducer(undefined, {})).toEqual(fromJS({}));
  });
});
