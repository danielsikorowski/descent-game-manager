import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the sidebarPanel state domain
 */

const selectSidebarPanelDomain = state =>
  state.get('sidebarPanel', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by SidebarPanel
 */

const makeSelectSidebarPanel = () =>
  createSelector(selectSidebarPanelDomain, substate => substate.toJS());

export default makeSelectSidebarPanel;
export { selectSidebarPanelDomain };
