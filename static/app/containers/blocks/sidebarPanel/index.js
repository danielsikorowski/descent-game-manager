/**
 *
 * SidebarPanel
 *
 */

import React, {PureComponent} from 'react';
import SidebarWrapper, {SidebarHeader, SidebarContainer} from './Wrapper';
import GameOptionsList from "../../common/GameOptionsList";

/* eslint-disable react/prefer-stateless-function */
export class SidebarPanel extends PureComponent {

  render() {
    return (
      <SidebarWrapper>
        <SidebarHeader>
          Rozpocznij przygodę!
        </SidebarHeader>
        <SidebarContainer>
          <GameOptionsList listType='sidebar'/>
        </SidebarContainer>
      </SidebarWrapper>
    );
  }
}

SidebarPanel.propTypes = {
};

export default SidebarPanel;
