/*
 * SidebarPanel Messages
 *
 * This contains all the text for the SidebarPanel component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.SidebarPanel.header',
    defaultMessage: 'This is SidebarPanel container !',
  },
});
