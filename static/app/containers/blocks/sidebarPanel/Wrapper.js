import styled from 'styled-components';

const SidebarPanelWrapper = styled.div`
  position: absolute;
  left: 300px;
  top: 50px;
  background-color: lightgray;
  border: 2px solid #b03030;
  height: 500px;
  width: 300px;
  transform: translateX(-300px);   
  -webkit-box-shadow: -1px 5px 11px 0 rgba(0,0,0,0.75);
  -moz-box-shadow: -1px 5px 11px 0 rgba(0,0,0,0.75);
  box-shadow: -1px 5px 11px 0 rgba(0,0,0,0.75);
`;

export const SidebarHeader = styled.h3`
  display: block;
  text-align: center;
  padding: 10px 0;
  height: 50px;
  font-size: 1.5rem;
  border-bottom: 1px solid white;
`;

export const SidebarContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 15px;  
  margin-top: 25px;
  
  & > button {
    border: 0;
    background-color: white;
    height: 35px;
    margin-bottom: 25px;
    
    &:hover {
      background-color: azure;
    }
    
    &:last-child {
      margin-bottom: 0;
    }
  }
`;

export default SidebarPanelWrapper;
