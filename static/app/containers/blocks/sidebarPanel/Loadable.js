/**
 *
 * Asynchronously loads the component for SidebarPanel
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
