import styled from 'styled-components';
import logoBackgroundImage from 'assets/images/descent-old-cracked-wall.jpg';

const AppLogoContainer = styled.div`
  background-image: url('${logoBackgroundImage}');
  background-position-x: center;
  border: 5px solid #c5c5c5;
`;
export const AppLogoWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  width: 280px;
  height: 100%;
  background: #000000ba;
  justify-content: center;
`;

export default AppLogoContainer;
