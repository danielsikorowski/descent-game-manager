/**
 *
 * AppLogo
 *
 */

import React from 'react';
import Wrapper, {AppLogoWrapper} from './Wrapper';
import DescentLogo from "../../../../components/common/DescentLogo/index";

function AppLogo() {
  return (
    <Wrapper>
      <AppLogoWrapper>
        <DescentLogo width='225px'/>
      </AppLogoWrapper>
    </Wrapper>
  );
}

export default AppLogo;
