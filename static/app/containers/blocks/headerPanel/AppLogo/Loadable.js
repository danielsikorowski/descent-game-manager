/**
 *
 * Asynchronously loads the component for AppLogo
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
