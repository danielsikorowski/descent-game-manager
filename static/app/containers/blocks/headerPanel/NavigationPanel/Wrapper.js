import styled from 'styled-components';

const NavigationPanelWrapper = styled.div`
  flex-grow: 3;
  padding: 0 30px;
  display: flex;
  align-items: center;
  
  .navbar {
    margin-bottom: 0;
    padding: 0 15px;
    
    .container {
      width: auto;
    }
  }
`;

export default NavigationPanelWrapper;
