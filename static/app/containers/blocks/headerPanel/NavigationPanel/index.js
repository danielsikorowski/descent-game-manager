/**
 *
 * NavigationPanel
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import Wrapper from './Wrapper';
import { MenuItem, Nav, Navbar, NavDropdown, NavItem } from "react-bootstrap";

function NavigationPanel() {
  return (
    <Wrapper>
      <Navbar>
        <Nav>
          <NavDropdown id="archives-list" title="Archiwum">
            <MenuItem>Potwory</MenuItem>
            <MenuItem>Bohaterowie</MenuItem>
            <MenuItem>Overlord</MenuItem>
          </NavDropdown>
          <NavItem>Ranking</NavItem>
          <NavItem>Forum</NavItem>
        </Nav>
      </Navbar>
    </Wrapper>
  );
}

NavigationPanel.propTypes = {};

export default NavigationPanel;
