/**
 *
 * UserPanel
 *
 */

import React, {PureComponent} from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectUserPanel from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import Wrapper from './Wrapper';
import UserInfoContainer from "./components/UserInfoContainer";
import UserOptionsDropdown from "./components/UserOptionsContainer";

class UserPanel extends PureComponent {

  render() {
    return (
      <Wrapper>
        <UserInfoContainer userdata={this.userData}/>
        <UserOptionsDropdown />
      </Wrapper>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  userData: makeSelectUserPanel(),
});

UserPanel.propTypes = {};
const withConnect = connect(mapStateToProps);

const withReducer = injectReducer({ key: 'userData', reducer });
const withSaga = injectSaga({ key: 'userData', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(UserPanel);
