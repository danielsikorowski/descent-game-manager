import { fromJS } from 'immutable';
import userPanelReducer from '../reducer';

describe('userPanelReducer', () => {
  it('returns the initial state', () => {
    expect(userPanelReducer(undefined, {})).toEqual(fromJS({}));
  });
});
