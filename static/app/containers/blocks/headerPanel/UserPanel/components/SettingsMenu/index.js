/**
 *
 * SettingsMenu
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import {ListGroup, ListGroupItem} from "react-bootstrap";

import CollapseWrapper, {ListGroupHeader, ListGroupFooter, ListGroupSeparator} from './Wrapper';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome/index.es";

function SettingsMenu(props) {
  return (
    <CollapseWrapper in={props.open}>
      <ListGroup>
        <ListGroupHeader/>
        <ListGroupItem href='#'>
          <FontAwesomeIcon icon='user-ninja'/> Profil
        </ListGroupItem>
        <ListGroupItem href='#'>
          <FontAwesomeIcon icon='user-cog'/> Ustawienia Konta
        </ListGroupItem>
        <ListGroupItem href='#'>
          <FontAwesomeIcon icon='tools'/> Ustawienia
        </ListGroupItem>
        <ListGroupItem href='#'>
          <FontAwesomeIcon icon='chart-line'/> Statystyki Użytkownika
        </ListGroupItem>
        <ListGroupSeparator/>
        <ListGroupItem onClick={props.logout}>
          <FontAwesomeIcon icon='power-off'/> Wyloguj się
        </ListGroupItem>
        <ListGroupFooter/>
      </ListGroup>
    </CollapseWrapper>
  );
}

SettingsMenu.propTypes = {};

export default SettingsMenu;
