import styled from 'styled-components';
import { Collapse } from "react-bootstrap";

const SettingsMenuCollapseWrapper = styled(Collapse)`
  position: relative;
  z-index: 1;
  left: -265px;
  top: 18px;
  width: 300px;
  -webkit-box-shadow: -1px 3px 11px 0 rgba(0,0,0,0.75);
  -moz-box-shadow: -1px 3px 11px 0 rgba(0,0,0,0.75);
  box-shadow: -1px 3px 11px 0 rgba(0,0,0,0.75);
  margin-bottom: 0;
  border: 1px solid #b03030;
  border-radius: 4px;
  
  > .list-group-item {
    border-left: 0;
    border-right: 0;
    
    :last-child {
      border-bottom: 0;
      border-bottom-left-radius: 0;
      border-bottom-right-radius: 0;
    }
  }
  
  &.in {
    left: -265px;
    top: 18px;
    width: 300px;
    z-index: 1;
    -webkit-box-shadow: -1px 3px 11px 0 rgba(0,0,0,0.75);
    -moz-box-shadow: -1px 3px 11px 0 rgba(0,0,0,0.75);
    box-shadow: -1px 3px 11px 0 rgba(0,0,0,0.75);
    margin-bottom: 0;
  }
`;

export const ListGroupHeader = styled.div`
  border-right: 0;
  border-left: 0;
  background: #b03030;
  height: 21px;
`;

export const ListGroupFooter = styled.div`
  border-right: 0;
  border-left: 0;
  background: #b03030;
  height: 10px;
`;

export const ListGroupSeparator = styled.div`
  border-right: 0;
  border-left: 0;
  background: #b03030;
  height: 5px;
`;

export default SettingsMenuCollapseWrapper;
