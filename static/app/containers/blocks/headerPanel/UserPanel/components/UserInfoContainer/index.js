/**
 *
 * UserInfo
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import Wrapper, {AvatarWrapper} from './Wrapper';
import UserAvatar from "components/common/UserAvatar";
import UserInformation from "../UserInformation";


function UserInfoContainer(props) {
  return (
    <Wrapper>
      <AvatarWrapper>
        <UserAvatar />
      </AvatarWrapper>
      <UserInformation />
    </Wrapper>
  );
}

UserInfoContainer.propTypes = {};

export default UserInfoContainer;
