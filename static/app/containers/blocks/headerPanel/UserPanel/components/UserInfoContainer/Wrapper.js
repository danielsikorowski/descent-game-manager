import styled from 'styled-components';

const UserInfoContainerWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  position: relative;
  height: inherit;
  width: inherit;
  background-color: inherit;
  z-index: 3;
`;

export const AvatarWrapper = styled.div`
  display: inline-block;
`;

export default UserInfoContainerWrapper;
