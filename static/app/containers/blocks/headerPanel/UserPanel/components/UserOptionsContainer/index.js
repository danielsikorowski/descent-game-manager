/**
 *
 * UserOptionsContainer
 *
 */

import React, {Component} from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import onClickOutside from "react-onclickoutside";

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import {logout} from "containers/root/AuthFlow/actions";
import Wrapper, {BottomButton} from './Wrapper';
import SettingsMenu from "../SettingsMenu/index";
import { compose } from "redux";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome/index.es";

class UserOptionsDropdown extends Component {
  state = {
    openMenu: false
  };

  openSettings = () => {
    this.setState(state => {
      return {openMenu: !state.openMenu};
    });
  };

  handleClickOutside = () => {
    this.setState(() => {
      return {openMenu: false};
    });
  };

  render() {
    return (
      <Wrapper>
        <BottomButton onClick={this.openSettings}>
          <FontAwesomeIcon icon='cog'/>
        </BottomButton>
        <SettingsMenu logout={this.props.logout} open={this.state.openMenu}/>
      </Wrapper>
    );
  }
}

UserOptionsDropdown.propTypes = {};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logout()),
  };
};

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
  onClickOutside
)(UserOptionsDropdown);
