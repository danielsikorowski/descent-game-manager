/*
 * UserOptionsContainer Messages
 *
 * This contains all the text for the UserOptionsContainer component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.UserOptionsDropdown.header',
    defaultMessage: 'This is the UserOptionsDropdown component !',
  },
});
