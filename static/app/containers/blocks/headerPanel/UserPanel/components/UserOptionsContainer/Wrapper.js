import styled from 'styled-components';

const UserOptionsContainerWrapper = styled.div`
    position: absolute;
    height: 35px;
    width: 35px;
    top: calc(100% - 35px);
    left: 100%;
    z-index: 2;
    border-radius: 0 50% 50% 0;
    -webkit-box-shadow: -1px 3px 11px 0 rgba(0,0,0,0.75);
    -moz-box-shadow: -1px 3px 11px 0 rgba(0,0,0,0.75);
    box-shadow: -1px 3px 11px 0 rgba(0,0,0,0.75);
    
`;

export const UserOptionButton = styled.button`
    text-align: center;
    height: inherit;
    width: inherit;
    font-size: 20px;
    padding: 2px;
    border: 0;
    border-radius: 0 50% 50% 0;
    color: white;
    background: #b03030;
`;

export const BottomButton = styled(UserOptionButton)`
    z-index: 2;
    position: absolute;
    bottom: 0;
    left: 0;
`;

export default UserOptionsContainerWrapper;
