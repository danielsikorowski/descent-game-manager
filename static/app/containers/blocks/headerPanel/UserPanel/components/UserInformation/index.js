/**
 *
 * UserInformation
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Wrapper, {UsernameBar, LevelBar, OtherInformations} from './Wrapper'

function UserInformation() {
  return (
    <Wrapper>
      <UsernameBar>Dezra the Vile</UsernameBar>
      <span>
        <LevelBar>Level - 2</LevelBar>
      </span>
      <OtherInformations>
      </OtherInformations>
    </Wrapper>
  );
}

UserInformation.propTypes = {};

export default UserInformation;
