import styled from 'styled-components';

const UserInformationWrapper = styled.div`
  width: 100%;
  height: 100%;
`;

export const UsernameBar = styled.div`
  margin-top: 5px;
  width: 100%;
  height: 20px;
  text-align: center;
  font-size: 20px;
  font-family: descent-title-bold;
`;

export const LevelBar = styled.div`
  text-align: center;
  font-size: 18px;
  font-family: descent-description;
`;

export const OtherInformations = styled.div`
  background: #f1f1f1;
  margin-top: 5px;
  height: 45px;
  width: 228px;   
  margin-left: auto;
  margin-right: auto;
  -webkit-box-shadow: inset 0px 3px 25px -5px rgba(0,0,0,0.75);
  -moz-box-shadow: inset 0px 3px 25px -5px rgba(0,0,0,0.75);
  box-shadow: inset 0px 3px 25px -5px rgba(0,0,0,0.75);
`;

export default UserInformationWrapper;
