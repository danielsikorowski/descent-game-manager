import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the userPanel state domain
 */

const selectUserPanelDomain = state =>
  state.get('userData', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by UserPanel
 */

const makeSelectUserPanel = () =>
  createSelector(selectUserPanelDomain, substate => substate.toJS());

export default makeSelectUserPanel;
export { selectUserPanelDomain };
