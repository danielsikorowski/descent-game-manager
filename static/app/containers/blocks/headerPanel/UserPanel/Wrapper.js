import styled from 'styled-components';

const UserPanelWrapper = styled.div`
  position: relative;
  margin-right: 35px;
  height: 100%;
  width: 335px;
  border: 1px solid gray;
  border-top: 0;
  background: lightgray;
  -webkit-box-shadow: -1px 3px 11px 0px rgba(0,0,0,0.75);
  -moz-box-shadow: -1px 3px 11px 0px rgba(0,0,0,0.75);
  box-shadow: -1px 3px 11px 0px rgba(0,0,0,0.75);
`;
export default UserPanelWrapper;
