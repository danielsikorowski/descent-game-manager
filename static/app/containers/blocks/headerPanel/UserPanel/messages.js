/*
 * UserPanel Messages
 *
 * This contains all the text for the UserPanel component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.UserPanel.header',
    defaultMessage: 'This is UserPanel container !',
  },
});
