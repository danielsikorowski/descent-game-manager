import styled from 'styled-components';
import {TITLE_BOLD} from '../../../../../assets/fonts/text'

export const SubmitButton = styled.button`
  font-family: ${TITLE_BOLD};
  font-size: 18px;
`;
