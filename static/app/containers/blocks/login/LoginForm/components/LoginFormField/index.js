/**
 *
 * LoginFormField
 *
 */

import React, { Fragment } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import ExtInput, {ExtInputConfig} from 'components/html/ExtInput';
// import { ExtInputUsername } from 'components/html/ExtInput/config';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

function LoginFormField(props) {
  const inputConfig = ExtInputConfig[props.purpose];

  return (
    <ExtInput
      config={inputConfig}
      value={props.value}
      onChange={props.onChange}
      {...inputConfig.defaultProps}>
      <FontAwesomeIcon icon={inputConfig.icon}/>
    </ExtInput>
  );
}

LoginFormField.propTypes = {};

export default LoginFormField;
