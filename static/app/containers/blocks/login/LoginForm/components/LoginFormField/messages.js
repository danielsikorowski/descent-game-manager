/*
 * LoginFormField Messages
 *
 * This contains all the text for the LoginFormField component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.LoginFormField.header',
    defaultMessage: 'This is the LoginFormField component !',
  },
});
