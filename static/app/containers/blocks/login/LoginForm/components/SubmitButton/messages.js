/*
 * SubmitButton Messages
 *
 * This contains all the text for the SubmitButton component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SubmitButton.header',
    defaultMessage: 'This is the SubmitButton component !',
  },
});
