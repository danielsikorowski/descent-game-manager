/**
 *
 * SubmitButton
 *
 */

import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button } from 'components/html';
import {TITLE_BOLD} from 'assets/fonts/text'

export const StyledSubmitButton = styled(Button)`
  font-family: ${TITLE_BOLD};
  font-size: 18px;
`;

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function SubmitButton() {
  return (
    <StyledSubmitButton className='btn btn-primary btn-block form__submit-btn' type='submit'>
      Enter the dungeon
      <FontAwesomeIcon icon="dungeon" style={{marginLeft: '8px'}}/>
    </StyledSubmitButton>
  );
}

SubmitButton.propTypes = {};

export default SubmitButton;
