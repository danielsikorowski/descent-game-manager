/**
 *
 * SubmitLoader
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

function SubmitLoader() {
  return (
    <FontAwesomeIcon icon="skull" className="fa-2x fa-spin" />
  );
}

SubmitLoader.propTypes = {};

export default SubmitLoader;
