/**
 *
 * LoginForm
 *
 */

import React from 'react';
import { extend, isNil } from 'lodash';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

import { connect } from "react-redux";
import Wrapper from "./Wrapper";

import SubmitButton from './components/SubmitButton';
import SubmitLoader from './components/SubmitLoader';
import RegisterLink from "../../../../components/common/RegisterLink";
import LoginFormField from "./components/LoginFormField";

import { getIsSendingLoginRequest, getAuthorizationError, getAuthorizationCredentials} from "../../../root/AuthFlow/selectors";
import { changeForm, loginRequest, clearError} from "../../../root/AuthFlow/actions";
import { createStructuredSelector } from "reselect/dist/reselect";

import ErrorMessage from 'components/common/ErrorMessage';

/* eslint-disable react/prefer-stateless-function */
class LoginForm extends React.Component {

  changeUsername = (event) => {
    const username = event.target.value;
    this.emitChange(extend(this.props.credentials, {username}));
  };

  changePassword = (event) => {
    const password = event.target.value;
    this.emitChange(extend(this.props.credentials, {password}));
  };

  emitChange = (credentials) => {
    const { dispatch } = this.props;
    dispatch(changeForm(credentials));
  };

  onSubmit = (e) => {
    e.preventDefault();
    const { username, password } = this.props.credentials;
    const { dispatch } = this.props;

    dispatch(clearError());

    if (this.validCredentials({username, password})) {
      dispatch(loginRequest({username, password}));
    }
  };

  validCredentials = ({username, password}) => {
    return !isNil(username) && !isNil(password);
  };

  render() {
    const { error, isSendingLoginRequest, credentials } = this.props;
    return (
      <Wrapper name="form" className="card" onSubmit={this.onSubmit}>
        <div className="card-header">
          You see a dark pit in front...
        </div>
        <div className="card-body">
          <div className="form-group">
            {error && <ErrorMessage error={error} />}
          </div>
          <div className="input-group form-group row no-gutters">
            <LoginFormField
              purpose="username"
              value={credentials.username}
              onChange={this.changeUsername} />
          </div>
          <div className='input-group form-group row no-gutters'>
            <LoginFormField
              purpose="password"
              value={credentials.password}
              onChange={this.changePassword}/>
          </div>
          <div className='form-group text-center form__submit-btn-wrapper'>
            {isSendingLoginRequest ? <SubmitLoader /> : <SubmitButton />}
          </div>
          <hr />
          <RegisterLink />
        </div>
      </Wrapper>
    );
  }
}

LoginForm.propTypes = {};

const mapStateToProps = createStructuredSelector({
  isSendingLoginRequest: getIsSendingLoginRequest(),
  error: getAuthorizationError(),
  credentials: getAuthorizationCredentials(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default withConnect(LoginForm);
