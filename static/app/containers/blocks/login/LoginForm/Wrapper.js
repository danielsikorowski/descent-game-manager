import styled from 'styled-components';
import {TITLE_BOLD} from '../../../../assets/fonts/text';

// todo: move header style into class
const LoginFormWrapper = styled.form`
  width: 330px;
  border: 1px solid gainsboro;
  text-align: center; 
  
  & .card-header {
    font-family: ${TITLE_BOLD};
    font-size: 20px;
    color: #006ca5;
    padding-left: 0;
    padding-right: 0;
  }
`;

export default LoginFormWrapper;
