/**
 *
 * RegisterForm
 *
 */

import React, { Fragment } from 'react';

// import PropTypes from 'prop-types';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';
import styled from 'styled-components';

import { connect } from 'react-redux';

import Wrapper from './Wrapper';
import DescentLogo from 'components/common/DescentLogo';
import ErrorMessage from 'components/common/ErrorMessage';

// import { RegisterFormWrapper } from './styled'

import { createStructuredSelector } from 'reselect/dist/reselect';

const DescentLogoWrapper = styled.div`
  margin-bottom: 25px;
`;

/* eslint-disable react/prefer-stateless-function */
class RegisterForm extends React.Component {

  onSubmit = () => {


  };

  render() {
    // const { error } = this.props;
    return (
      <Fragment>
        <DescentLogoWrapper>
          <DescentLogo height="120px" />
        </DescentLogoWrapper>
        <Wrapper name="form" className="card" onSubmit={this.onSubmit}>
          <div className="card-header text-center">Register as a Hero and go on the journey of your life...</div>
          <div className="card-body">
            <div className="form-group">
              {/*{error && <ErrorMessage error={error} />}*/}
              <div className="alert alert-danger">
                Fill all required form fields
              </div>
            </div>
            <div className="input-group form-group row no-gutters">

            </div>
          </div>
          <div className="card-footer">
            <button className="btn btn-block btn-success" disabled>Submit Form to the Hero Association</button>
          </div>
        </Wrapper>
      </Fragment>
    );
  }
}

RegisterForm.propTypes = {};

const mapStateToProps = createStructuredSelector({
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(RegisterForm);
