/*
 * RegisterForm Messages
 *
 * This contains all the text for the RegisterForm component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.RegisterForm.header',
    defaultMessage: 'This is RegisterForm container !',
  },
});
