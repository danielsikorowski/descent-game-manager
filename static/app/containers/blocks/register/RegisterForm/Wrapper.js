import styled from 'styled-components';
import {TITLE_STANDARD, TITLE_BOLD} from "../../../../assets/fonts/text";

const LoginFormWrapper = styled.form`
  width: 60vw;
  margin: 0 auto;
  height: 100vh;
  background-color: white;
  
    & .card-header {
    font-family: ${TITLE_BOLD};
    font-size: 23px;
    color: #006ca5;
    padding-left: 0;
    padding-right: 0;
  }
  
  & .card-footer {
    font-family: ${TITLE_STANDARD};
  }
`;

export default LoginFormWrapper;
