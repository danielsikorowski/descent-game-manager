import { fromJS } from 'immutable';
import registerFormReducer from '../reducer';

describe('registerFormReducer', () => {
  it('returns the initial state', () => {
    expect(registerFormReducer(undefined, {})).toEqual(fromJS({}));
  });
});
