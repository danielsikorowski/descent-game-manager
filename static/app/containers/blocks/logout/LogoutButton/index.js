/**
 *
 * LogoutButton
 *
 */

import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {logout} from "../../../root/AuthFlow/actions";

function LogoutButton(props) {
  return (
    <div>
      <button onClick={props.logout}>
        logout
      </button>
    </div>
  );
}


const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logout()),
  };
};

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(LogoutButton);
