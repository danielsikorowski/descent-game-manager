import {extend} from "lodash";

const defaultSettings = {
  gameSidebarPanel: true
};
const setupRoutingConfig = settings => extend({}, defaultSettings, settings);



/**
 *
 *  Private Routing Configuration objects
 *  each path has its own config extended with default settings
 *
 * */

export const guildhallConfig = setupRoutingConfig({
  // gameSidebarPanel: false
});
