
export const ROUTING_STATES = {
  GUILDHALL: '/guildhall',
  LOGIN: '/login',
  REGISTER: '/register',
  DEFAULT: '/',
  NEWGAME: '/game-app/new',
  LOADGAME: '/load-game/' // used both in find game and load game
};
