import styled from 'styled-components';

const AppWrapper = styled.div`
  max-width: 100%;
  min-height: 100%;
  display: flex;
  flex-direction: column;
`;

export default AppWrapper;
