/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route, Redirect } from 'react-router-dom';

// Wrappers
import AuthFlow from 'containers/root/AuthFlow';
import LayoutManager from 'global/layout';

// View State Pages
import NotFound from '../../../containers/root/ViewStates/NotFoundPage/Loadable';
import Login from '../../../containers/root/ViewStates/LoginPage/Loadable';
import Register from '../../../containers/root/ViewStates/RegisterPage/Loadable';
import Guildhall from '../../../containers/root/ViewStates/GuildhallPage/Loadable';

// Routes
import PrivateRoute from 'components/routing/PrivateRoute';
import PublicRoute from 'components/routing/PublicRoute';

// Routing Configuration
import {guildhallConfig} from '../Configuration/privateRouting/index';

// Modals
import ModalWindowContainer from "../../common/ModalWindowContainer";

export default function App() {
  return (
    <AuthFlow>
      <Helmet
        titleTemplate="%s - Descent Manager"
        defaultTitle="Descent Game Manager">
        <meta
          name="description"
          content="support application for playing Descent: Journeys in the dark 2nd edition."/>
      </Helmet>
      <Switch>
        <Route path="/" exact render={() => <Redirect to="/guildhall" />} />
        <PrivateRoute config={guildhallConfig} path="/guildhall" component={Guildhall} />
        <PublicRoute path="/login" component={Login} />
        <PublicRoute path="/register" component={Register} />
        <PublicRoute component={NotFound} />
      </Switch>
      <ModalWindowContainer />
    </AuthFlow>
  );
}
