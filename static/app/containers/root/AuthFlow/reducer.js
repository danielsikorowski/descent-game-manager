/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import {CHANGE_FORM, CLEAR_ERROR, REQUEST_ERROR, SENDING_REQUEST, SET_AUTH} from './constants';
import auth from "./auth";

// The initial state of the App
export const initialState = fromJS({
  credentials: {
    username: '',
    password: ''
  },
  error: '',
  isSendingLoginRequest: false,
  isLoggedIn: auth.isLoggedIn()
});

function loginFlowReducer (state = initialState, action) {
  switch (action.type) {
    case CHANGE_FORM:
      return state.set('credentials', fromJS(action.credentials));
    case SET_AUTH:
      return state.set('isLoggedIn', fromJS(action.newAuthState));
    case SENDING_REQUEST:
      return state.set('isSendingLoginRequest', fromJS(action.sending));
    case REQUEST_ERROR:
      return state.set('error', fromJS(action.error));
    case CLEAR_ERROR:
      return state.set('error', fromJS(''));
    default:
      return state
  }
}

export default loginFlowReducer;
