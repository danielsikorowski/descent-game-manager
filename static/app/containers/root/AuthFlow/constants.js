/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const SENDING_REQUEST = 'app/LoginFlow/SENDING_REQUEST';
export const LOGIN_REQUEST = 'app/LoginFlow/LOGIN_REQUEST';
export const REGISTER_REQUEST = 'app/LoginFlow/REGISTER_REQUEST';
export const SET_AUTH = 'app/LoginFlow/SET_AUTH';
export const LOGOUT = 'app/LoginFlow/LOGOUT';
export const CHANGE_FORM = 'app/LoginFlow/CHANGE_FORM';
export const REQUEST_ERROR = 'app/LoginFlow/REQUEST_ERROR';
export const CLEAR_ERROR = 'app/LoginFlow/CLEAR_ERROR';
