/**
 *
 * App Login Flow Wrapper
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import AppWrapper from 'containers/root/app/Wrapper';
import saga from './saga';
import reducer from "./reducer";
import compose from "redux/src/compose";

function AuthFlowWrapper(props) {
  return (
    <AppWrapper>
      {props.children}
    </AppWrapper>
  );
}

AuthFlowWrapper.propTypes = {};

const withReducer = injectReducer({ key: 'auth', reducer });
const withSaga = injectSaga({ key: 'auth', saga });

export default compose(
  withReducer,
  withSaga
)(AuthFlowWrapper);
