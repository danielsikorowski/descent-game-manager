import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the dashboard state domain
 */

const selectAuthFlow = state => state.get('auth', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Dashboard
 */

const selectAuthFlowRaw = () =>
  createSelector(selectAuthFlow, substate => substate.toJS());

const getIsSendingLoginRequest = () =>
  createSelector(selectAuthFlow, state => state.get('isSendingLoginRequest'));

const getAuthorizationError = () =>
  createSelector(selectAuthFlow,state => state.get('error'));

const getAuthorizationCredentials = () =>
  createSelector(selectAuthFlow, state => state.get('credentials').toJS());

export default selectAuthFlowRaw;
export {
  selectAuthFlow,
  getIsSendingLoginRequest,
  getAuthorizationError,
  getAuthorizationCredentials
};
