import request from 'fakeServer/fakeRequest'

let localStorage;

// If we're testing, use a local storage polyfill
// if (global.process && process.env.NODE_ENV === 'test') {
//   localStorage = require('localStorage')
// } else {
  // If not, use the browser one
  localStorage = global.window.localStorage;
// }

export default class Auth{
  /**
  * Logs a user in, returning a promise with `true` when done
  * @param  {string} username The username of the user
  * @param  {string} password The password of the user
  */
  static login (username, password) {
    if (Auth.isLoggedIn()) return Promise.resolve(true);

    // Post a fake request
    return request.post('/login', {username, password})
      .then(response => {
        // Save token to local storage
        localStorage.token = response.token;
        return Promise.resolve(true);
      })
  }
  /**
  * Logs the current user out
  */
  static logout () {
    return request.post('/logout');
  }

  /**
  * Checks if a user is logged in
  */
  static isLoggedIn () {
    return !!localStorage.token;
  }

  /**
  * Registers a user and then logs them in
  * @param  {string} username The username of the user
  * @param  {string} password The password of the user
  */
  static register (username, password) {
    // Post a fake request
    return request.post('/register', {username, password})
      // Log user in after registering
      .then(() => Auth.login(username, password))
  }

  onChange () {}
}
