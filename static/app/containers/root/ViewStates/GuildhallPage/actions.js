/*
 *
 * Dashboard actions
 *
 */

import { DEFAULT_ACTION, TEST_ACTION } from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function test(message) {
  return {
    type: TEST_ACTION,
    message
  };
}
