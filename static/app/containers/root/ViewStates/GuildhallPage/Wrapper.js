import styled from 'styled-components';

const GuildhallPageWrapper = styled.div`
  width: 100%;
  height: inherit;
`;

export default GuildhallPageWrapper;
