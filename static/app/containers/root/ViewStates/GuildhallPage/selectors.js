import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the dashboard state domain
 */

const selectGuildhallDomain = state => state.get('guildhall', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Guildhall
 */

const makeSelectGuildhall = () =>
  createSelector(selectGuildhallDomain, substate => substate.toJS());

export default makeSelectGuildhall;
export { selectGuildhallDomain };
