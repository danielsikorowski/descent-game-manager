/**
 *
 *  Guildhall
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectGuildhall from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import Wrapper from './Wrapper';

function Guildhall() {
  return (
    <Wrapper>
      <Helmet>
        <title>Guildhall</title>
        <meta name="description" content="Description of Guildhall" />
      </Helmet>
      test
    </Wrapper>
  );
}

Guildhall.propTypes = {
};

const mapStateToProps = createStructuredSelector({
  guildhall: makeSelectGuildhall()
});

function mapDispatchToProps(dispatch) {
  return {
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'guildhall', reducer });
const withSaga = injectSaga({ key: 'guildhall', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Guildhall);
