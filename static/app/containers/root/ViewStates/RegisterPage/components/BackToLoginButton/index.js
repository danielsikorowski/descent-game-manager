/**
 *
 * BackToLoginButton
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { Button } from 'components/html'
const Wrapper = styled.div`
  position: fixed;
  background-color: white;
  padding: 10px 25px;
  top: 50px;
  left: 0;
`;

const BackToLoginBtn = styled(Button)`
  & a {
    color: white;
    
    &:hover {
      text-decoration: none;
    }
  }
`;

function BackToLoginButton() {
  return (
    <Wrapper>
      <BackToLoginBtn type="link" className="btn btn-dark" to="/login">
        Go Back to <br/> Dungeon entrance
      </BackToLoginBtn>
    </Wrapper>
  );
}

BackToLoginButton.propTypes = {};

export default BackToLoginButton;
