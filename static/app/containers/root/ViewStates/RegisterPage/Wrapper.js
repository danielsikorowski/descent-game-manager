import styled from 'styled-components';
import BackgdImage from 'assets/images/descent-background.jpg';

const RegisterPageWrapper = styled.div`
  width: 100%;
  height: 100vh;
  padding-top: 25px;
  padding-bottom: 50px;
  overflow-y: scroll;
  background: url('${BackgdImage}') no-repeat top;
  background-size: cover;
`;

export default RegisterPageWrapper;
