import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the loginPage state domain
 */

const getLoginPage = state => state.get('loginPage', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by LoginPage
 */

const selectLoginPageRaw = () =>
  createSelector(getLoginPage, substate => substate.toJS());

const selectLoggingIn = () =>
  createSelector(getLoginPage, state => state.get('loggingIn'));

export default getLoginPage;
export { selectLoginPageRaw, selectLoggingIn};
