import styled from 'styled-components';
import BackgdImage from 'assets/images/descent-background.jpg';

const LoginPageWrapper = styled.div`
  width: 100%;
  margin: 0 auto;
  height: 100vh;
  padding-top: 250px;
  background: url('${BackgdImage}') no-repeat top;
  background-size: cover;
`;

export default LoginPageWrapper;
