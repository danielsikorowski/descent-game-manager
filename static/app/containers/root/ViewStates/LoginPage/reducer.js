/*
 *
 * LoginPage reducer
 *
 */

// import { compose, onAction, init, merge } from 'redux-hor';
import { fromJS } from 'immutable';
import { DEFAULT_ACTION } from './constants';

export const initialState = fromJS({
  login: 'test'
});
//
// export default compose(
//   onAction(DEFAULT_ACTION, state => merge(state))
// )(init(initialState));

function loginPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default loginPageReducer;
