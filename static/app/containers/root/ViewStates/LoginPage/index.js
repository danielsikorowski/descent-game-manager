/**
 *
 * LoginPage
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import auth from 'containers/root/AuthFlow/auth';
// import {makeSelectIsLoggingIn} from './selectors';
import reducer from './reducer';
import saga from './saga';
import Wrapper from './Wrapper';
import DescentLogo from 'components/common/DescentLogo';
// import {loginRequest} from "../../root/AuthFlow/actions";
import LoginForm from "containers/blocks/login/LoginForm";

/* eslint-disable react/prefer-stateless-function */
export class LoginPage extends React.PureComponent {
  state = {
    redirectToReferrer: auth.isLoggedIn(),
  };

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }

    return (
      <Wrapper>
        <Helmet>
          <title>Log In</title>
          <meta name="description" content="Description of LoginPage" />
        </Helmet>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-3 offset-md-2">
              <LoginForm />
            </div>
            <div className="col-md-7">
              <DescentLogo />
            </div>
          </div>
        </div>
      </Wrapper>
    );
  }
}

LoginPage.propTypes = {};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'loginPage', reducer });
const withSaga = injectSaga({ key: 'loginPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LoginPage);
