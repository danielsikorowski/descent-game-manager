/**
 *
 * ModalWindowContainer
 *
 */

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectReducer from 'utils/injectReducer';
import {selectModalWindowsList} from './selectors';
import reducer from './reducer';
import { ModalWindow } from "./components/ModalWindow";
import { closeModal } from "./actions";

/* eslint-disable react/prefer-stateless-function */
export class ModalWindowContainer extends React.Component {
  getModalClose = (id) => () => {
    this.props.dispatch(closeModal(id))
  };
  render() {
    const { modalWindowsList } = this.props;
    if (!modalWindowsList) {
      return null;
    }
    return (
      <Fragment>
        {
          modalWindowsList.map(modalWindowProps => {
            const {id} = modalWindowProps;
            return <ModalWindow
              key={id}
              onModalClose={this.getModalClose(id)}
              configProps={modalWindowProps}/>
          })
        }
      </Fragment>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  modalWindowsList: selectModalWindowsList(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: 'modalWindowContainer', reducer });

export default compose(
  withReducer,
  withConnect
)(ModalWindowContainer);
