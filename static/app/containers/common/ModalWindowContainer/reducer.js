/*
 *
 * ModalWindowContainer reducer
 *
 */
import { fromJS, List } from 'immutable';
import { MODAL_OPEN_NEW_GAME, CLOSE_MODAL } from './constants';

export const initialState = fromJS({
  modalWindowsList: List()
});

function modalWindowContainerReducer(state = initialState, action) {
  switch (action.type) {
    case MODAL_OPEN_NEW_GAME:
      return state.update('modalWindowsList', modalsList => modalsList.push(action.modalWindow));
    case CLOSE_MODAL:
      return state.update('modalWindowsList', modalsList => modalsList.filter(modal => modal.id !== action.modalId));
    default:
      return state;
  }
}

export default modalWindowContainerReducer;
