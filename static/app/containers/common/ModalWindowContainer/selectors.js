import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the modalWindowContainer state domain
 */

const selectModalWindowContainerDomain = state =>
  state.get('modalWindowContainer', initialState);

/**
 * Other specific selectors
 */

const selectModalWindowsList = () =>
  createSelector(selectModalWindowContainerDomain, substate => substate.get('modalWindowsList').toJS());

/**
 * Default selector used by ModalWindowContainer
 */

const makeSelectModalWindowContainer = () =>
  createSelector(selectModalWindowContainerDomain, substate => substate.toJS());

export default makeSelectModalWindowContainer;
export { selectModalWindowsList, selectModalWindowContainerDomain };
