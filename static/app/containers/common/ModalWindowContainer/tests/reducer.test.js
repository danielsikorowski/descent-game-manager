import { fromJS } from 'immutable';
import modalWindowContainerReducer from '../reducer';

describe('modalWindowContainerReducer', () => {
  it('returns the initial state', () => {
    expect(modalWindowContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});
