/*
 *
 * ModalWindowContainer actions
 *
 */

import { CLOSE_MODAL } from './constants';

export function closeModal(id) {
  return {
    type: CLOSE_MODAL,
    modalId: id
  };
}
