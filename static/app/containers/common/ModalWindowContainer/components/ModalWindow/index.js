/**
 *
 * ModalWindow
 *
 */

import React from 'react';
import ReactModal from 'react-responsive-modal';
import PropTypes from 'prop-types';

/* eslint-disable react/prefer-stateless-function */
export class ModalWindow extends React.Component {
  render() {
    const { configProps, onModalClose } = this.props;
    const { isModalOpen, id } = configProps;

    return (
      <div>
        <ReactModal
          modalId={id}
          open={isModalOpen}
          onClose={onModalClose}
          center>
          <h2>Simple centered Modal!</h2>
        </ReactModal>
      </div>
    );
  }
}

ModalWindow.propTypes = {
  onModalClose: PropTypes.func.isRequired,
  configProps: PropTypes.object.isRequired
};

export default ModalWindow;
