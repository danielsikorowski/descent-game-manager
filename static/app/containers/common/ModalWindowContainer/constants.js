/*
 *
 * ModalWindowContainer constants
 *
 */

export const MODAL_OPEN_NEW_GAME = 'app/ModalWindowContainer/MODAL_OPEN_NEW_GAME';
export const CLOSE_MODAL = 'app/ModalWindowContainer/CLOSE_MODAL';
