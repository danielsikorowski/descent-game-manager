export const NEW_GAME = 'app/new-game';
export const LOAD_GAME = 'app/load-game';
export const SAVE_GAME = 'app/save-game';
export const SEARCH_GAME = 'app/search-game';
export const EXIT_GAME = 'app/exit-game';
