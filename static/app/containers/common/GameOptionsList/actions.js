/*
 *
 * Game Options List actions
 *
 */

import uuid from "uuid/v4";

import { MODAL_OPEN_NEW_GAME } from '../ModalWindowContainer/constants';
import { NEW_GAME, LOAD_GAME, SAVE_GAME, SEARCH_GAME, EXIT_GAME } from './constants';
import { newGameModal } from "../ModalWindowContainer/modalTypes";

export function createNewGame(windowMode = false) {
  if (windowMode) {
    return {
      type: MODAL_OPEN_NEW_GAME,
      modalWindow: {
        id: uuid(),
        modalType: newGameModal,
        isModalOpen: true
      }
    };
  }
  return {
    type: NEW_GAME,
  };
}

export function loadYourGame(windowMode = false) {
  return {
    type: LOAD_GAME,
    windowMode
  };
}


export function saveYourGame() {
  return {
    type: SAVE_GAME
  };
}

export function searchForExistingGame(windowMode = false) {
  return {
    type: SEARCH_GAME,
    windowMode
  }
}

export function exitCurrentGame() {
  return {
    type: EXIT_GAME
  }
}
