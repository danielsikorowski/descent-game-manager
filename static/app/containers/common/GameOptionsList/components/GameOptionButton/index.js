/**
 *
 * GameOptionButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome/index.es";

function GameOptionButton(props) {
  const { config }= props;
  return (
    <button onClick={config.onClick} className={config.className}>
      {config.icon && <FontAwesomeIcon icon={config.icon}/>} {config.label}
    </button>
  );
}

GameOptionButton.propTypes = {
  config: PropTypes.object.isRequired
};

export default GameOptionButton;
