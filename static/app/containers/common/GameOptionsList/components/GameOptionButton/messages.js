/*
 * GameOptionButton Messages
 *
 * This contains all the text for the GameOptionButton component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.GameOptionButton.header',
    defaultMessage: 'This is the GameOptionButton component !',
  },
});
