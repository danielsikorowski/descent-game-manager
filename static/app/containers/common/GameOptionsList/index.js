/**
 *
 * GameOptionsList
 *
 */

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import configuration from './config'

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { connect } from "react-redux";
import GameOptionButton from "./components/GameOptionButton/index";


class GameOptionsList extends PureComponent {

  getOnClickHandler = (optionConfig) => {
    optionConfig.onClick = () => {
      this.props.dispatch(optionConfig.getAction());
    }
  };

  getClassName = (optionConfig) => {
    optionConfig.className = `game-option-btn`;
  };

  render() {
    return configuration.gameOptionsList[this.props.listType]
      .map((optionConfig, keyId) => {
        this.getOnClickHandler(optionConfig);
        this.getClassName(optionConfig);
        return <GameOptionButton key={keyId} config={optionConfig}/>
      });
  };
}

GameOptionsList.propTypes = {
  listType: PropTypes.string.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(null, mapDispatchToProps)(GameOptionsList);
