/*
 * GameOptionsList Messages
 *
 * This contains all the text for the GameOptionsList component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.GameOptionsList.header',
    defaultMessage: 'This is the GameOptionsList component !',
  },
});
