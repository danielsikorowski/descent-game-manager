import {createNewGame, loadYourGame, saveYourGame, searchForExistingGame, exitCurrentGame} from './actions';

const GameOptionsListConfig = {
  gameOptionsList: {
    sidebar: [
      {
        label: 'New Game',
        icon: 'scroll',
        getAction: () => createNewGame(true)
      },
      {
        label: 'Load Game',
        icon: 'journal-whills',
        getAction: () => loadYourGame(true)
      },
      {
        label: 'Find Game',
        icon: 'search-location',
        getAction: () => searchForExistingGame(true)
      },
    ],
    guildhall: [
      {
        label: 'New Game',
        getAction: () => createNewGame()
      },
      {
        label: 'Load Game',
        getAction: () => loadYourGame()
      },
      {
        label: 'Find Game',
        getAction: () => searchForExistingGame()
      }
    ],
    inGameMenu: [
      {
        label: 'New Game',
        getAction: () => createNewGame(true)
      },
      {
        label: 'Save Game',
        getAction: () => saveYourGame()
      },
      {
        label: 'Exit Game',
        getAction: () => exitCurrentGame()
      }
    ]
  }
};

export default GameOptionsListConfig;
